<<<<<<< HEAD
=======
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <title>Katas 3</title>
</head>

<body>
    <h1>Katas 3</h1>
    <div id="content"></div>
    <script>
        let kataWrapper = document.createElement("div")
        const array = [469, 755, 244, 245, 758, 450, 302, 20,
            712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];
        function oneThroughTwenty() {
            const numbers = [];
            for (let counter = 1; counter <= 20; counter++) {
                numbers.push(counter)
            }
            writeToDOM("One Through Twenty", numbers);
        }
        function evensToTwenty() {
            const numbers = [];
            for (let counter = 2; counter <= 20; counter += 2) {
                numbers.push(counter);
            }
            writeToDOM("Evens To Twenty", numbers);
        }
        function oddsToTwenty() {
            const numbers = []
            for (let counter = 1; counter <= 20; counter += 2) {
                numbers.push(counter)
            }
            writeToDOM("Odds To Twenty", numbers)
        }
        function multiplesOfFive() {
            const numbers = []
            for (let counter = 5; counter <= 100; counter += 5) {
                numbers.push(counter)
            }
            writeToDOM("Multiples Of Five", numbers);
        }
        function squareNumbers() {
            const numbers = []
            for (let counter = 1; counter <= 10; counter++) {
                numbers.push(counter * counter)
            }
            writeToDOM("Square Numbers", numbers);
        }
        function countingBackwards() {
            const numbers = []
            for (let counter = 20; counter > 0; counter--) {
                numbers.push(counter)
            }
            writeToDOM("Counting Backwards", numbers);
        }
        function evenNumbersBackwards() {
            const numbers = [];
            for (let counter = 20; counter > 0; counter -= 2) {
                numbers.push(counter)
            }
            writeToDOM("Even Numbers Backwards", numbers);
        }
        function oddNumbersBackwards() {
            const numbers = [];
            for (let counter =19; counter >= 0; counter -= 2) {
                numbers.push(counter);
            }
            writeToDOM("Odd Numbers Backwards", numbers);
        }
        function multiplesOfFiveBackwards() {
            const numbers = [];
            for (let counter = 100; counter >= 5; counter -= 5) {
                numbers.push(counter);
            }
            writeToDOM("Multiples Of Five Backwards", numbers)
        }
        function squareNumbersBackwards() {
            const numbers = [];
            for (let counter = 10; counter >= 0; counter--) {
                numbers.push(counter);
            }
            writeToDOM("Square Numbers Backwards", numbers)
        }
        function sampleArray() {
            writeToDOM("Sample Array", array);
        }
        let even = function (num) {
            return num % 2 === 0;
        }
        function sampleArrayEvens() {
            let evenArray = array.filter(even);
            writeToDOM("Sample Array Evens", evenArray)
        }
        let odd = function (num) {
            return num % 2 === 1;
        }
        function sampleArrayOdds() {
            let oddArray = array.filter(odd);
            writeToDOM("Sample Array Odds", oddArray)
        }
        function sampleArraySquared() {
            numbers = [];
            for (let counter = 0; counter < array.length; counter++) {
                numbers.push(array[counter] * array[counter]);
            }
            writeToDOM("Sample Array Squared", numbers)
        }
        function sumOfOneThroughTwenty() {
            const array2 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17,
                18, 19, 20];
            let sum = 0;
            for (let counter = 0; counter < array.length; counter++) {
                sum += array2[counter];
            }
            writeToDOM("Sum Of One Through Twenty", sum)
        }
        function sumOfArray() {
            let sum = 0;
            for (let counter = 0; counter < array.length; counter++) {
                sum += array[counter];
            }
            writeToDOM("Sum Of Array", sum)
        }
        function smallestElement() {
            let arrayMin = Math.min(469, 755, 244, 245, 758, 450, 302, 20,
                712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472)
            writeToDOM("Smallest Element in Sample Array", arrayMin)
        }
        function largestElement() {
            let arrayMax = Math.max(469, 755, 244, 245, 758, 450, 302, 20,
                712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472)
            writeToDOM("Largest Element in Sample Array", arrayMax)
        }

        function writeToDOM(title, numbers) {
            let div = document.createElement("div");
            let heading = document.createElement("h3");
            let titleText = document.createTextNode(title);
            let answerText = document.createTextNode(numbers);
            heading.appendChild(titleText);
            div.appendChild(heading);
            div.appendChild(answerText);
            kataWrapper.appendChild(div);
        }
        var destination = document.getElementById("content");
        destination.appendChild(kataWrapper);
        oneThroughTwenty();
        evensToTwenty();
        oddsToTwenty();
        multiplesOfFive();
        squareNumbers();
        countingBackwards();
        evenNumbersBackwards();
        oddNumbersBackwards();
        multiplesOfFiveBackwards();
        squareNumbersBackwards();
        sampleArray();
        sampleArrayEvens();
        sampleArrayOdds();
        sampleArraySquared();
        sumOfOneThroughTwenty();
        sumOfArray();
        smallestElement();
        largestElement();
    </script>
</body>

</html>
>>>>>>> efd0bcac910f10de780e7acba7175b64fb000f8a
